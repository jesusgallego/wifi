package es.ua.eps.wifi;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    private static final int WIFI_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE}, WIFI_PERMISSION);
        } else {
            getInfo();
        }
    }

    private void getInfo() {

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        WifiInfo info = wifiManager.getConnectionInfo();
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();

        String ssid = info.getSSID();
        String bssid = info.getBSSID();
        int frecuencia = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            frecuencia = info.getFrequency();
        }
        int velocidad = info.getLinkSpeed();
        String unidades = WifiInfo.LINK_SPEED_UNITS;
        int fuerza = wifiManager.calculateSignalLevel(info.getRssi(), 5);
        String oculta = info.getHiddenSSID() ? "Si" : "No";
        String ip = Formatter.formatIpAddress(dhcpInfo.ipAddress);
        String puertaEnlace = Formatter.formatIpAddress(dhcpInfo.gateway);
        String mascara = Formatter.formatIpAddress(dhcpInfo.netmask);

        String text = String.format("SSID: %s \n"
                        + "BSSID: %s\n"
                        + "Frecuencia: %d\n"
                        + "Velocidad: %d %s\n"
                        + "Fuerza: %d/5\n"
                        + "Oculta?: %s\n"
                        + "IP: %s\n"
                        + "Puerta de enlace: %s\n"
                        + "Máscara: %s",
                ssid,
                bssid,
                frecuencia,
                velocidad,
                unidades,
                fuerza,
                oculta,
                ip,
                puertaEnlace,
                mascara
        );

        textView.setText(text);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WIFI_PERMISSION:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getInfo();
                }
                break;
        }
    }
}
